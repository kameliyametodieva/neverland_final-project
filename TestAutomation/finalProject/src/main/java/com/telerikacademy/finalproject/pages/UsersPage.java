package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

public class UsersPage extends BasePage {

    public UsersPage(){super("users.url");}

    public final String searchField = "users.Field.search";
    public final String searchButton = "users.Button.search";
    public final String connectButton = "users.Button.Connect";
    public final String sentRejectButton = "users.Button.SentRequestReject";
    public final String latestPostsButton = "navigation.LatestPosts";
    public final String logoutButton = "navigation.Logout";
    public final String confirmButton = "users.Button.Confirm";
    public final String disconnectButton = "users.Button.Disconnect";

    public void assertPageLoaded(){
        assertPageNavigated();
    }

    public void searchUser(String usernameKey, String userLocator) {
        String username = Utils.getConfigPropertyByKey(usernameKey);
        actions.isElementPresentUntilTimeout(searchField, 20);
        actions.typeValueInField(username, searchField);
        actions.isElementPresentUntilTimeout(searchButton, 15);
        actions.clickElement(searchButton);
        actions.isElementPresentUntilTimeout(userLocator, 15);
    }
    public void sendConnectionRequest(String usernameLocator ){
        actions.isElementPresentUntilTimeout(usernameLocator, 20);
        actions.clickElement(usernameLocator);
        actions.isElementPresentUntilTimeout(connectButton, 20);
        actions.clickElement(connectButton);
    }

    public void assertRequestForConnectionIsSent(){
        actions.assertElementPresent(sentRejectButton);
    }

    public void withdrawConnectionRequest(String usernameLocator ){
        actions.isElementPresentUntilTimeout(usernameLocator, 20);
        actions.clickElement(usernameLocator);
        actions.isElementPresentUntilTimeout(sentRejectButton, 20);
        actions.clickElement(sentRejectButton);
    }

    public void assertWithdrawOfConnectionRequest(){
        actions.assertElementPresent(connectButton);
    }

    public void confirmConnectionRequest (String usernameLocator ){
        HomePage homePage = new HomePage();
        homePage.actions.hoverOnElement(homePage.usersButton);
        actions.isElementPresentUntilTimeout(homePage.requestsButton, 15);
        actions.clickElement(homePage.requestsButton);
        actions.isElementPresentUntilTimeout(usernameLocator, 20);
        actions.clickElement(usernameLocator);
        UsersPage userPage = new UsersPage();
        actions.isElementPresentUntilTimeout(userPage.connectButton,25);
        actions.clickElement(userPage.confirmButton);
    }

    public void assertConfirmationOfConnectionRequest() {
        actions.assertElementPresent(disconnectButton);
    }

    public void disconnectUser(String usernameLocator ){
        actions.isElementPresentUntilTimeout(usernameLocator, 20);
        actions.clickElement(usernameLocator);
        actions.isElementPresentUntilTimeout(disconnectButton, 10);
        actions.clickElement(disconnectButton);
    }

    public void assertDisconnection(){
        actions.assertElementPresent(connectButton);
    }

    public void logoutUser(){
        actions.isElementPresentUntilTimeout(logoutButton, 25);
        actions.clickElement(logoutButton);
    }

    public void assertUserLogout(){
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
    }
}
