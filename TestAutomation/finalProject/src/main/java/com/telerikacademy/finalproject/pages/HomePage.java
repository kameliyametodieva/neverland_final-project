package com.telerikacademy.finalproject.pages;

public class HomePage extends BasePage {

    public HomePage() {
        super("base.url");
    }

    public final String logoutButton ="navigation.Logout";
    public final String latestPostButton = "homePage.Posts";
    public final String newPostButton = "homePage.NewPost";
    public final String settingsButton = "homePage.Settings";
    public final String nationalitiesButton = "homePage.Nationalities";
    public final String usersButton = "homePage.Users";
    public final String requestsButton= "homePage.Requests";
    public final String AdminPosts = "homePage.Admin";
    public final String categoryButton = "homePage.Category";

    public void assertPageLoaded() {
        actions.isElementPresentUntilTimeout(logoutButton, 15);
        assertPageNavigated();
    }

    public void clickLogout() {
        actions.clickElement(logoutButton);
    }
}
