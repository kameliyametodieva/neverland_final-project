package com.telerikacademy.finalproject.pages;

import java.util.concurrent.TimeUnit;

public class EditPostPage extends BasePage{

    public EditPostPage() {
        super("post.url");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    String postSearchField = "post.searchField";
}
