package com.telerikacademy.finalproject.pages;

import java.util.Random;

public class CreatePostPage extends BasePage{
    public CreatePostPage() {

        super("createPost.url");
    }
    public final String newPostTitleField  ="newPost.Field.Title";
    public final String newPostTitleText = "Neverland Selenium Title";
    public final String newPostDescriptionField = "newPost.Field.Description";
    public final String newPostDescriptionText = "Selenium description";
    public final String newPostCategoriesArea = "newPost.Categories.Area";
    public final String categoryName = "drinks";
    //public final String secondPositionCategory = "newPost.Categories.option";
    public final String newPostSaveButton = "newPost.Button.Save";

}
