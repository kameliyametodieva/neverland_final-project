package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class LatestPostsPage extends BasePage {

    public LatestPostsPage() {
        super("latestPosts.url");
    }

    public final String latestPostTitle = "latestPosts.PostTitle";

    public void assertLatestPostTitle(String expectedTitle) {
        List<WebElement> elements = actions.getDriver().findElements(By.xpath(Utils.getUIMappingByKey("latestPost.AllPosts")));
        WebElement firstPostTitle = elements.get(0).findElement(By.xpath(Utils.getUIMappingByKey("latestPosts.PostTitle")));
        String firstPostTitleText = firstPostTitle.getText();
        Assert.assertEquals(expectedTitle, firstPostTitleText);
    }
}
