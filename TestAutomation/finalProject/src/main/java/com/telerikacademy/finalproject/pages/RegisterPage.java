package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import javax.swing.*;

public class RegisterPage extends BasePage {

    public RegisterPage () {
        super("register.url");
    }

    public final String registerTitle = "register.Title";
    public final String userNameField = "register.Field.Username";
    public final String userAgeField = "register.Field.Age";
    public final String passwordField = "register.Field.Password";
    public final String confirmPasswordField = "register.Field.PasswordConfirm";
    public final String firstNameField = "register.Field.FirstName";
    public final String lastNameFiled = "register.Field.LastName";
    public final String nationalityField = "register.Field.Nationality";
    public final String genderField = "register.Field.Gender";
    public final String aboutYouField = "register.Field.AboutYou";
    public final String visibilityPictureField = "register.Field.Visibility";
    public final String pictureButton = "register.Button.ChooseFile";
    public final String registerButton = "register.Button.Register";
    public final String homePageLink = "register.Link.HomePage";
    public final String loginPageLink = "register.Link.LoginPage";


    public void registerUser_WithMandatoryFields (String username, String password, String confirmPassword,
                                                  String firstName, String lastName, int visibility) {
        String emailRegistr = Utils.getConfigPropertyByKey(username);
        String passwordRegistr = Utils.getConfigPropertyByKey(password);
        String confirmPasswordRegistr = Utils.getConfigPropertyByKey(confirmPassword);
        actions.isElementPresentUntilTimeout(registerTitle, 15);
        actions.typeValueInField(emailRegistr, userNameField);
        actions.typeValueInField(passwordRegistr, passwordField);
        actions.typeValueInField(confirmPasswordRegistr, confirmPasswordField);
        actions.typeValueInField(firstName, firstNameField);
        actions.typeValueInField(lastName, lastNameFiled);
        actions.clickElement(visibilityPictureField);

    }
}
