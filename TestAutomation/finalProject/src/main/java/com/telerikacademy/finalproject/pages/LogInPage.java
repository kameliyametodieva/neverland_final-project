package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

public class LogInPage extends BasePage {

    public LogInPage() {
        super("login.url");
    }

    public final String userNameField = "login.Field.Username";
    public final String passwordField = "login.Field.Password";

    //todo better locator
    public final String loginButton = "login.Button";
    public final String wrongLoginMessage = "loginPage.Wrong.Message";


    public void loginUser (String usernameKey, String passwordKey) {
        String username = Utils.getConfigPropertyByKey(usernameKey);
        String password = Utils.getConfigPropertyByKey(passwordKey);
        actions.isElementPresentUntilTimeout(userNameField, 20);
        actions.typeValueInField(username, userNameField);
        actions.typeValueInField(password, passwordField);
        actions.clickElement("login.Button");
        HomePage homePage = new HomePage();
        actions.assertElementPresent(homePage.logoutButton);
    }

}
