package testCases.adminFunctionality;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoryTests extends BaseTest {
    public final String file = "image.upload";


    @BeforeClass
    public static void loginAdmin_After_fillingAdminNameAndPassword() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUser("usernameAdmin", "passwordAdmin");


    }

    @Test
    public void a_createCategory() {
        HomePage homePage = new HomePage();
        actions.hoverOnElement(homePage.settingsButton);
        actions.clickElement(homePage.categoryButton);
        CreateCategoryPage categoryPage = new CreateCategoryPage();
        categoryPage.assertPageNavigated();
        actions.assertElementPresent("category.pageTitle");
        actions.isElementPresentUntilTimeout("category.addButton", 10);
        actions.clickElement("category.addButton");
        //actions.typeValueInField("image.upload","category.emoticon");
        //actions.typeValueInField(Utils.getUIMappingByKey("category.name"), "category.description");
        //actions.typeValueInField("image.upload", "category.emoticon");
        Utils.getConfigPropertyByKey(file);




    }
    @Test
    public void b_editCategory() {
        HomePage homePage = new HomePage();
        actions.hoverOnElement(homePage.settingsButton);
        actions.clickElement(homePage.categoryButton);
        CreateCategoryPage categoryPage = new CreateCategoryPage();
        categoryPage.assertPageNavigated();
        actions.assertElementPresent("category.pageTitle");

    }


}