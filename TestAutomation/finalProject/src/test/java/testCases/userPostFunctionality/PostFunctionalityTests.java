package testCases.userPostFunctionality;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import testCases.BaseTest;

import java.util.List;

public class PostFunctionalityTests extends BaseTest {

    @BeforeClass
    public static void loginUser_After_fillingUserNameAndPassword() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("username", "password");
    }

    @Test
    public void loggedUser_creates_NewPublicPost() {
        HomePage homePage = new HomePage();
        actions.hoverOnElement(homePage.latestPostButton);
        actions.clickElement(homePage.newPostButton);
        CreatePostPage newPost = new CreatePostPage();
        newPost.assertPageNavigated();
        actions.assertElementPresent(newPost.newPostTitleField);
        actions.typeValueInField(newPost.newPostTitleText, newPost.newPostTitleField);
        actions.typeValueInField(newPost.newPostDescriptionText, newPost.newPostDescriptionField);
        actions.clickElementInList(newPost.newPostCategoriesArea, newPost.categoryName);
        actions.clickElement(newPost.newPostSaveButton);
        UsersPage users = new UsersPage();
        users.assertPageNavigated();
        actions.clickElement(users.latestPostsButton);
        LatestPostsPage latestPosts = new LatestPostsPage();
        latestPosts.assertPageNavigated();
        actions.isElementPresentUntilTimeout("latestPost.AllPosts", 15);
        latestPosts.assertLatestPostTitle(newPost.newPostTitleText);
    }
}
