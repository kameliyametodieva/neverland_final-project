package testCases;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.RegisterPage;
import org.jsoup.Connection;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RegistrationTests extends BaseTest {



    public RegisterPage registerPage;

    @Before
    public void beforeTest(){
        registerPage = new RegisterPage();
        registerPage.navigateToPage();
        registerPage.assertPageNavigated();
    }

    @Test
    public void verifyRegisterFields_ArePresent() {
        actions.assertElementPresent(registerPage.registerTitle);
        actions.assertElementPresent(registerPage.userNameField);
        actions.assertElementPresent(registerPage.userAgeField);
        actions.assertElementPresent(registerPage.passwordField);
        actions.assertElementPresent(registerPage.confirmPasswordField);
        actions.assertElementPresent(registerPage.firstNameField);
        actions.assertElementPresent(registerPage.lastNameFiled);
        actions.assertElementPresent(registerPage.nationalityField);
        actions.assertElementPresent(registerPage.genderField);
        actions.assertElementPresent(registerPage.aboutYouField);
        actions.assertElementPresent(registerPage.visibilityPictureField);
        actions.assertElementPresent(registerPage.pictureButton);
        actions.assertElementPresent(registerPage.registerButton);
        actions.assertElementPresent(registerPage.homePageLink);
        actions.assertElementPresent(registerPage.loginPageLink);
    }

    @Test
    public void navigateFromRegisterPage_ToHomePage_and_Verify_SignUpButton() {
        actions.clickElement(registerPage.homePageLink);
        NavigationPage navPage = new NavigationPage();
        navPage.assertPageNavigated();
        actions.clickElement(navPage.signUpButton);
        registerPage.assertPageNavigated();
       }

    @Test
    public void navigateFromRegisterPage_ToLoginPage() {
        actions.clickElement(registerPage.loginPageLink);
        NavigationPage navPage = new NavigationPage();
        navPage.assertPageNavigated();
    }
}

