***Prerequisites to run bat file for selenium tests***
1.Install Maven 
   or type $mvn -version in the cmd to see if it is already installed.
   
2.Navigate to TestAutomation/finalProject and run the fillowing file:

SeleniumRunner.bat

4.Navigate to 
neverland_final-project\TestAutomation\finalProject\target\site\surefire-report.htm to check the result

***After running requirеments***
1.Open with "Intelij" (with java version 11) the neverland_final-project and navigate to \TestAutomation\finalProject\src\tet\resources\mapping\ui_map.properties
and to change the value of the:

nationalities.newNameNationality = newNationality

nationalities.nationalityIsUpdated = //h3[contains(text(), 'newNationalityUpdate')]

nationalities.nationalityUpdateName = newNationalityUpdate 
