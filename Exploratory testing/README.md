# **Final Project QA Telerik Academy - "Healthy Food Social Network"**
## Team NEVERLAND

# This folder contains documents for Exploratory testing on Social Network API using Xray Exploratory App

Exploratory Test - [Search user/s](https://drive.google.com/file/d/18L3Hu6FHY1NDXdVjDmj3ucJrmNcWj7Xy/view?usp=sharing)

Exploratory Test - [Search users and Connection](https://drive.google.com/file/d/13ibzLEdXgt9wumxSoo2tgU1HGKgpWJCx/view?usp=sharing)

Exploratory Test - [Login functionality](https://drive.google.com/file/d/1StQySBiH048P4OIT_evF2MXrXNXT-elp/view?usp=sharing)

Exploratory Test - [New user registration](https://drive.google.com/file/d/149oVvmzM9vOhohicyIZ-yq7SjaPsSzi8/view?usp=sharing)

Exploratory Test - [Post functionality](https://drive.google.com/file/d/1CktGPCGz2FQ790anNPZRbTaje1Iq9hGX/view?usp=sharing)

Exploratory Test - [Search and sort functionality](https://drive.google.com/file/d/1_G-JNnY3TL-qJCjf8BSz1rfpTdpbu4Nf/view?usp=sharing)

Exploratory Test - [Comment functionality](https://drive.google.com/file/d/1yx0kbsaFfVCr8PdkT66JNl7zEK4QIQdl/view?usp=sharing)

Exploratory Test - [Admin functionality](https://drive.google.com/file/d/1t4-xZSQuwH6kjYXONJ7kx0ERtXJq9TMs/view?usp=sharing)
