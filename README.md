# **Final Project QA Telerik Academy - "Healthy Food Social Network"**
## Team NEVERLAND

# Project on QA activities prepared by Kameliya Metodieva, Latinka Raycheva and Nina Vodenicharska

# [Trello board QA Neverland Team](https://trello.com/b/REdilux7/final-project)


# [TEST PLAN QA proccess](https://drive.google.com/file/d/1DQiYEpiCamDAiHg5h-abZnjxG0-2wpZh/view?usp=sharing)

# [Test Cases ](https://drive.google.com/file/d/1KTNhYzOjUHV3Vnwtb9ORzKc0bcXd-xEH/view?usp=sharing)

# Exploratory Test - [Files](https://gitlab.com/kameliyametodieva/neverland_final-project/-/tree/master/Exploratory%20testing)

# [Test Execution Report](https://drive.google.com/file/d/1y77wFugTi4CGYmK21qDdiIkeFquWa-og/view?usp=sharing)

# Logged Issues - [Git Repo](https://gitlab.com/kameliyametodieva/neverland_final-project/-/issues)

# [Bug Report](https://drive.google.com/file/d/11Ia5kDCSoaOWt9r46sWRQU4karCG3kgc/view?usp=sharing)

# [Test Summary Charts](https://drive.google.com/file/d/1yoSzr7sN1WWJhg3oqPlu0Ndyq9AeOeU0/view?usp=sharing)